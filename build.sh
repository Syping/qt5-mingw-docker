#!/bin/bash
set +e
docker pull debian:buster-slim
docker build -t qt5-shared-mingw-openssl qt5-shared-mingw
docker tag qt5-shared-mingw-openssl syping/qt5-shared-mingw-openssl:5.12.8
docker tag qt5-shared-mingw-openssl syping/qt5-shared-mingw-openssl:5.12.x
docker tag qt5-shared-mingw-openssl syping/qt5-shared-mingw-openssl:latest
docker push syping/qt5-shared-mingw-openssl:5.12.8
docker push syping/qt5-shared-mingw-openssl:5.12.x
docker push syping/qt5-shared-mingw-openssl:latest
docker build -t qt5-static-mingw-openssl qt5-static-mingw
docker tag qt5-static-mingw-openssl syping/qt5-static-mingw-openssl:5.12.8
docker tag qt5-static-mingw-openssl syping/qt5-static-mingw-openssl:5.12.x
docker tag qt5-static-mingw-openssl syping/qt5-static-mingw-openssl:latest
docker push syping/qt5-static-mingw-openssl:5.12.8
docker push syping/qt5-static-mingw-openssl:5.12.x
docker push syping/qt5-static-mingw-openssl:latest
